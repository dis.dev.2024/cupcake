"use strict"

let isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

const isWebp = () => {
    // Проверка поддержки webp
    function testWebP(callback) {
        let webP = new Image();
        webP.onload = webP.onerror = function () {
            callback(webP.height == 2);
        };
        webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
    }
    // Добавление класса _webp или _no-webp для HTML
    testWebP(function (support) {
        let className = support === true ? 'webp' : 'no-webp';
        document.body.classList.add(className);
    });
}

/* Добавление класса touch для HTML если браузер мобильный */
const addTouchClass = () => {
    // Добавление класса _touch для HTML если браузер мобильный
    if (isMobile.any()) document.documentElement.classList.add('touch');
}

// Добавление loaded для HTML после полной загрузки страницы
const addLoadedClass = () => {
    window.addEventListener("load", function () {
        setTimeout(function () {
            document.documentElement.classList.add('loaded');
        }, 0);
    });
}

const fullVHfix = () => {
    const fullScreens = document.querySelectorAll('[data-fullscreen]');
    if (fullScreens.length && isMobile.any()) {
        window.addEventListener('resize', fixHeight);
        function fixHeight() {
            let vh = window.innerHeight * 0.01;
            document.documentElement.style.setProperty('--vh', `${vh}px`);
        }
        fixHeight();
    }
}

//  Smooth Scroll
const SmoothScroll = (element) => {
    const smoothLinks = document.querySelectorAll(element);
    for (let smoothLink of smoothLinks) {
        smoothLink.addEventListener('click', function (e) {
            e.preventDefault();
            const id = smoothLink.getAttribute('href');

            document.querySelector(id).scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            });
        });
    };
}

const Tabs = () => {
    document.addEventListener('click', (event) => {
        const tabsArray = document.querySelectorAll('[data-tabs]');
        if (tabsArray.length > 0) {
            if (event.target.closest('[data-tabs-nav]'))  {
                const tabs = event.target.closest('[data-tabs]');
                const tabsNavs = tabs.querySelectorAll('[data-tabs-nav]');
                const tabsContent = tabs.querySelectorAll('[data-tabs-target]');
                const tabsPath = event.target.closest('[data-tabs-nav]').dataset.tabsNav;

                tabsNavs.forEach(elem => {
                    elem.classList.remove('active');
                });
                tabsContent.forEach(elem => {
                    elem.classList.remove('active');
                });
                tabs.querySelector(`[data-tabs-nav="${tabsPath}"]`).classList.add('active');
                tabs.querySelector(`[data-tabs-target="${tabsPath}"]`).classList.add('active');
            }
        }
    });
};

const Quantity = () => {

    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-quantity-button]')) {
            const quantity = event.target.closest('[data-quantity]')
            const quantityInput = quantity.querySelector('[data-quantity-input]')
            const quantityMin = quantityInput.min
            const quantityMax = quantityInput.max
            const quantityDirection = event.target.closest('[data-quantity-button]').dataset.quantityButton
            let quantityValue = 0;

            if (!quantityInput.disabled) {
                if(quantityDirection === 'minus') {
                    quantityValue = parseInt(quantityInput.value) - 1
                    if (quantityValue < quantityMin) {
                        quantityValue = quantityMin
                    }
                }
                else {
                    quantityValue = parseInt(quantityInput.value) + 1
                    if (quantityValue > quantityMax) {
                        quantityValue = quantityMax
                    }
                }
                quantityInput.value = quantityValue
            }
        }
    })

    if( document.querySelector('[data-quantity-input]')) {

        document.querySelector('[data-quantity-input]').addEventListener('change', function (){
            let val = parseInt(this.value)
            const quantity = this.closest('[data-quantity]').querySelector('[data-quantity-input]')

            if(!val) {
                val = quantity.min
            }

            if (val < quantity.min) {
                val = quantity.min
            }
            else if (val > quantity.max) {
                val = quantity.max
            }

            quantity.value = val
        })
    }
}

const Nav = () => {
    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-nav-toggle]')) {
            document.querySelector('body').classList.toggle('nav-open')
            document.querySelectorAll('[data-nav-children]').forEach(elem => {
                elem.classList.remove('active');
            });
            document.querySelector('[data-nav-secondary]').classList.remove('open')
            document.querySelector('#up').scrollIntoView({
                behavior: 'auto',
                block: 'start'
            });
            return false
        }
        else {
            if (document.body.classList.contains('nav-open')) {
                if (!event.target.closest('[data-nav]')) {
                    document.querySelector('body').classList.remove('nav-open')
                    document.querySelectorAll('[data-nav-children]').forEach(elem => {
                        elem.classList.remove('active');
                    });
                    document.querySelector('[data-nav-secondary]').classList.remove('open')
                }
            }
        }
    })
    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-nav-parent]')) {
            const navChild = event.target.closest('[data-nav-parent]').dataset.navParent;
            document.querySelector(`[data-nav-children="${navChild}"]`).classList.add('active');
            document.querySelector('[data-nav-secondary]').classList.add('open')
        }
    })
    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-nav-back]')) {
            document.querySelectorAll('[data-nav-children]').forEach(elem => {
                elem.classList.remove('active');
            });
            document.querySelector('[data-nav-secondary]').classList.remove('open')
        }
    })
};

const Spoilers = () => {

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-spoiler-control]')) {
            const spoilerItem       = event.target.closest('[data-spoiler]');
            const spoilerContent    = spoilerItem.querySelector('[data-spoiler-content]');
            const spoilerIsOpen     = spoilerItem.classList.contains('open');


            if (event.target.closest('[data-spoilers]')) {
                if (!spoilerIsOpen) {
                    event.target.closest('[data-spoilers]').querySelectorAll('[data-spoiler]').forEach(elem => {

                        if (elem.classList.contains('open')) {
                            slidetoggle.hide(
                                elem.querySelector('[data-spoiler-content]'),
                                {
                                    miliseconds: 200,
                                }
                            )
                            elem.classList.remove('open')
                        }
                    });

                    slidetoggle.show(
                        spoilerContent,
                        {
                            miliseconds: 200,
                        }
                    )
                    spoilerItem.classList.add('open')
                }
                else {

                }
            }
            else  {
                slidetoggle.toggle(
                    spoilerContent,
                    {
                        miliseconds: 200,
                    }
                )
                spoilerItem.classList.toggle('open')
            }
        }
    })
}

const customSelect = () => {
    if (document.querySelectorAll('[data-select]').length > 0) {

        document.addEventListener('click', (event) => {
            if (!event.target.closest('[data-select].disabled')) {
                if (event.target.closest('[data-select-toggle]')) {
                    let selectContainer = event.target.closest('[data-select]');
                    if (selectContainer.classList.contains('open')) {
                        selectContainer.classList.remove('open');
                    }
                    else {
                        selectClose();
                        selectContainer.classList.add('open');
                    }
                }
                else {
                    if (!event.target.closest('[data-select-content]')) {
                        selectClose();
                    }
                }
                function selectClose() {
                    document.querySelectorAll('[data-select]').forEach(el => {
                        el.classList.remove('open');
                    });
                }
            }

        });

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-select-item]')) {
                console.log('select')
                const select        = event.target.closest('[data-select]')
                const selectActive  = select.querySelector('[data-select-active]')
                const selectValue   = select.querySelector('[data-select-value]')

                let selectItem= event.target.closest('[data-select-item]').dataset.selectItem

                select.querySelectorAll('[data-select-item]').forEach(elem => {
                    elem.classList.remove('active');
                });
                event.target.closest('[data-select-item]').classList.add('active')
                select.classList.remove('open')
                selectActive.innerHTML = selectItem
                selectValue.value = selectItem
            }
        });
    }
}

const Lng = () => {

    document.addEventListener('click', (event) => {
        const lng = document.querySelectorAll('[data-lng]');
        if (lng.length > 0) {
            if (event.target.closest('[data-lng-toggle]')) {
                let lngContainer = event.target.closest('[data-lng]');
                if (lngContainer.classList.contains('open')) {
                    lngContainer.classList.remove('open');
                }
                else {
                    lngClose();
                    lngContainer.classList.add('open');
                }
            }
            else {
                if (!event.target.closest('[data-lng-content]')) {
                    lngClose();
                }
            }
            function lngClose() {
                lng.forEach(el => {
                    el.classList.remove('open');
                });
            }

            document.addEventListener('click', (event) => {
                if (event.target.closest('[data-lng-item]')) {
                    let itemValue = event.target.closest('[data-lng-item]').innerHTML

                    event.target.closest('[data-lng]').querySelectorAll('[data-lng-item]').forEach(elem => {
                        elem.classList.remove('active');
                    });
                    event.target.closest('[data-lng-item]').classList.add('active')
                    event.target.closest('[data-lng]').classList.remove('open')
                    event.target.closest('[data-lng]').querySelector('[data-lng-active]').innerHTML = itemValue
                }
            })
        }
    });
};

const Search = () => {
    if (document.querySelector('[data-search]')) {
        const search = document.querySelector('[data-search]')
        const searchFiled = document.querySelector('[data-search-field]')
        const searchResult = document.querySelector('[data-search-result]')
        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-search-toggle]')) {
                search.classList.add('open')
                document.body.classList.add('search-open')
            }
            else {
                if (search.classList.contains('open')) {
                    if (!event.target.closest('[data-search]')) {
                        searchResult.classList.remove('open');
                        search.classList.remove('open')
                        searchFiled.value = ''
                        document.body.classList.remove('search-open')
                    }
                }
            }
        });

        searchFiled.addEventListener('keyup', (event) => {
            let searchValue = event.currentTarget.value;
            console.log(searchValue)

            if (searchValue.length > 2) {
                searchResult.classList.add('open');
            }
            else {
                searchResult.classList.remove('open');
            }

        })
    }
};

const FileField = () => {
    window.addEventListener("load", function (e) {
        if (document.querySelectorAll('[data-file]').length > 0) {
            const fileInputs = document.querySelectorAll('[data-file]')
            fileInputs.forEach(el => {
                let fileInput = el.querySelector('[data-file-input]')
                let filePlaceholder = el.querySelector('[data-file-placeholder]')
                let fileName = ''

                fileInput.addEventListener('change', (event) => {
                    fileName = fileInput.value.replace(/^.*[\\\/]/, '')
                    if (fileName) {
                        filePlaceholder.innerHTML = fileName
                        el.classList.add('file-field--uploaded')
                    }
                    else {
                        filePlaceholder.innerHTML = filePlaceholder.dataset.filePlaceholder
                        el.classList.remove('file-field--uploaded')
                    }
                })
            })
        }
    });
}

const initSliders = () => {
    // Перечень слайдеров

    if (document.querySelector('[data-clients]')) {
        new Swiper('[data-clients]', {
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 2,
            spaceBetween: 16,
            speed: 600,
            navigation: {
                nextEl: '[data-clients-next]',
                prevEl: '[data-clients-prev]',
            },
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
                1230: {
                    slidesPerView: 4,
                    spaceBetween: 30,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-primary]')) {
        new Swiper('[data-primary]', {
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 600,
            autoplay: {
                delay: 60000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '[data-primary-pagination]',
                clickable: true,
            },
            breakpoints: {
                1230: {
                    slidesPerView: 1,
                    spaceBetween: 30,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-selection]')) {
        new Swiper('[data-selection-slider]', {
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 5,
            spaceBetween: 0,
            speed: 600,
            navigation: {
                nextEl: '[data-selection-next]',
                prevEl: '[data-selection-prev]',
            },
            breakpoints: {
                576: {
                    slidesPerView: 6,
                    spaceBetween: 0,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 0,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 0,
                },
                1230: {
                    slidesPerView: 8,
                    spaceBetween: 2,
                },
            },
            on: {}
        });

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-selection-item]')) {
                event.target.closest('[data-selection]').querySelectorAll('[data-selection-item]').forEach(elem => {
                    elem.classList.remove('active');
                });
                event.target.closest('[data-selection-item]').classList.add('active')
            }
        })
    }

    if (document.querySelector('[data-card]')) {
        new Swiper('[data-card]', {
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 20,
            speed: 600,
            navigation: {
                nextEl: '[data-card-next]',
                prevEl: '[data-card-prev]',
            },
            on: {}
        });
    }

    if (document.querySelector('[data-items]')) {
        new Swiper('[data-items]', {
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 2,
            spaceBetween: 16,
            speed: 600,
            navigation: {
                nextEl: '[data-items-next]',
                prevEl: '[data-items-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
                1230: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-portfolio]')) {
        new Swiper('[data-portfolio]', {
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            initialSlide: 3,
            spaceBetween: 16,
            speed: 600,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
            },
            navigation: {
                nextEl: '[data-portfolio-next]',
                prevEl: '[data-portfolio-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 20,
                },
                1170: {
                    slidesPerView: 4,
                    spaceBetween: 30,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-cleaning]')) {
        new Swiper('[data-cleaning]', {
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 16,
            speed: 600,
            navigation: {
                nextEl: '[data-cleaning-next]',
                prevEl: '[data-cleaning-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 20,
                },
                1170: {
                    slidesPerView: 'auto',
                    spaceBetween: 30,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-certificates]')) {
        new Swiper('[data-certificates]', {
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 44,
            speed: 600,
            navigation: {
                nextEl: '[data-certificates-next]',
                prevEl: '[data-certificates-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 0,
                }
            },
            on: {
                slideChange: function () {
                    let num = this.slides.length - this.realIndex
                    if (num === 2) {
                        console.log(num)
                        document.querySelector('[data-certificates-next]').classList.add('hidden');
                    }
                    else {
                        document.querySelector('[data-certificates-next]').classList.remove('hidden');
                    }
                },
            }
        });
    }

    if (document.querySelector('[data-shop]')) {
        new Swiper('[data-shop]', {
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 20,
            speed: 600,
            navigation: {
                nextEl: '[data-shop-next]',
                prevEl: '[data-shop-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 30,
                }
            },
            on: {
            }
        });
    }

    if (document.querySelector('[data-cakes]')) {

        const productThumbs = new Swiper('[data-cakes]', {
            observer: true,
            loop: false,
            observeParents: true,
            watchSlidesProgress: true,
            slidesPerView: 'auto',
            spaceBetween: 12,
            speed: 600,
            grid: {
                rows: 1,
            },
            scrollbar: {
                el: '[data-cakes-scrollbar]',
            },
            pagination: {
                el: '[data-cakes-pagination]',
                clickable: true,
            },
            navigation: {
                nextEl: '[data-cakes-next]',
                prevEl: '[data-cakes-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                    slidesPerGroup: 3,
                    grid: {
                        rows: 2,
                    },
                }
            },
        })
    }

    if (document.querySelector('[data-modal-gallery]')) {
        new Swiper('[data-modal-gallery]', {
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 20,
            speed: 600,
            navigation: {
                nextEl: '[data-modal-gallery-next]',
                prevEl: '[data-modal-gallery-prev]',
            },
            on: {
            }
        });
    }

    if (document.querySelector('[data-decor]')) {

        new Swiper('[data-decor]', {
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 12,
            speed: 600,
            navigation: {
                nextEl: '[data-decor-next]',
                prevEl: '[data-decor-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    spaceBetween: 12,
                }
            },
            on: {
            }
        });

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-decor-item]')) {
                event.target.closest('[data-decor]').querySelectorAll('[data-decor-item]').forEach(elem => {
                    elem.classList.remove('active')
                })
                event.target.closest('[data-decor-item]').classList.add('active')
                event.target.closest('[data-info-group]').nextElementSibling.classList.remove('disabled')
            }
        })
    }
}

const rangeInit = () => {

    const dataSliders = document.querySelectorAll('[data-range-slider]');
    if (dataSliders.length > 0) {
        dataSliders.forEach(el => {
            let sliderStart = el.getAttribute('data-start').split(",").map(parseFloat);
            let sliderMin = parseFloat(el.dataset.min);
            let sliderMax = parseFloat(el.dataset.max);
            let slideWeight = []

            noUiSlider.create(el, {
                start: sliderStart,
                step: 1,
                range: {
                    'min': sliderMin,
                    'max': sliderMax,
                },
                format: wNumb({
                    decimals: 0
                }),
            });
            if (el.dataset.weight) {
                slideWeight = el.dataset.weight.split(",");
                el.noUiSlider.on('update', function (values) {
                    let handle = el.querySelector('.noUi-handle')
                    let amount = parseInt(values.join(' :: ')) - 1
                    let valMin = parseInt(handle.getAttribute('aria-valuemin'))
                    let valMax = parseInt(handle.getAttribute('aria-valuemax'))
                    let valNow = parseInt(handle.getAttribute('aria-valuenow'))

                    console.log(valNow)

                    handle.setAttribute('data-amount', slideWeight[amount])
                    if (valNow < valMax && valMin < valNow) {
                        handle.classList.add('view-amount')

                    }
                    else {
                        handle.classList.remove('view-amount')
                    }
                    if (valNow >= 6) {
                        document.querySelector('[data-filling-second]').classList.add('filling-second-open')
                    }
                    else {
                        document.querySelector('[data-filling-second]').classList.remove('filling-second-open')
                    }

                    if (valNow >= 50) {
                        console.log('>50')
                    }
                    else {
                        console.log('<50')
                    }
                });
            }


            el.noUiSlider.on('update', function (values) {
                let handle = el.querySelector('.noUi-handle')
                let valNow = parseInt(handle.getAttribute('aria-valuenow'))
                const elem = '<span>' + '<b>></b>50' + '</span>'

                if (valNow >= 50) {
                    console.log('>50')
                    document.querySelector('.noUi-handle').classList.add('after-hide')
         //           document.querySelector('.noUi-touch-area').classList.add('after-hide')
                    document.querySelector('.noUi-touch-area').innerHTML = elem
                }
                else {
                    console.log('<50')
                    document.querySelector('.noUi-handle').classList.remove('after-hide')
                    document.querySelector('.noUi-touch-area').innerHTML = ''
                }
            });
        });
    }
}

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-person-change]')) {
        setTimeout(() => {
            let count = event.target.closest('[data-quantity]').querySelector('[data-quantity-input]').value
            console.log(count)
            if (count >= 6) {
                document.querySelector('[data-filling-second]').classList.add('filling-second-open')
            }
            else {
                document.querySelector('[data-filling-second]').classList.remove('filling-second-open')
            }
        }, 500);
    }
})

if (document.querySelector('[data-person-quantity]')) {
    let count = document.querySelector('[data-person-quantity]')
    count.addEventListener('change', function (event) {
        console.log(event.target.value)
    })
}

const product = () => {
    if (document.querySelector('[data-product]')) {
        const productCard   = document.querySelector('[data-product]')
        const productImage  = document.querySelector('[data-product-image]')
        const productBase   = document.querySelector('[data-product-base]')
        const productItems  = document.querySelector('[data-product-items]')

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-product-choice]')) {
                productBase.classList.add('hide')
                productItems.classList.remove('hide')
            }
        })

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-product-check]')) {
                event.preventDefault()
                let productImageData = event.target.closest('[data-product-check]').dataset.productCheck
                let productImagePath = '<img src="' + productImageData +'" class="img-cover" alt="">'

                productCard.querySelectorAll('[data-product-check]').forEach(elem => {
                    elem.classList.remove('added');
                });
                event.target.closest('[data-product-check]').classList.toggle('added')
                productImage.innerHTML = productImagePath
                productCard.classList.add('selected')
                productBase.classList.remove('hide')
                productItems.classList.add('hide')
            }
        })
    }

    if (document.querySelector('[data-info-group="decor"]')) {

        document.querySelector('[data-info-group="decor"]').querySelectorAll('input[type="radio"]').forEach(el => {
            el.addEventListener('change', (event) => {
                event.target.closest('[data-info-group]').nextElementSibling.classList.remove('disabled')
            })
        })
    }

    if (document.querySelector('[data-info-group="holiday"]')) {

        document.querySelector('[data-info-group="holiday"]').querySelectorAll('input[type="radio"]').forEach(el => {
            el.addEventListener('change', (event) => {
                event.target.closest('[data-info-group]').nextElementSibling.classList.remove('disabled')
            })
        })
    }

    if (document.querySelector('[data-check-group]')) {
        document.querySelector('[data-check-group]').querySelectorAll('input[type="radio"]').forEach(elem => {
            elem.addEventListener('change', (event) => {
                console.log(elem.value)
                if (elem.value > 0) {
                    event.target.closest('[data-check-group]').querySelector('[data-select]').classList.remove('disabled')
                }
                else {
                    event.target.closest('[data-check-group]').querySelector('[data-select]').classList.add('disabled')
                }
            })
        })
    }

    if (document.querySelector('[data-payment-types]')) {
        document.querySelector('[data-payment-types]').querySelectorAll('[data-payment-type]').forEach(el => {
            el.addEventListener('change', (event) => {
                console.log(el.value)
                if (el.value === 'individual') {
                    el.closest('[data-payment-types]').querySelector('.form-group-hide').classList.remove('hide')
                }
                else {
                    el.closest('[data-payment-types]').querySelector('.form-group-hide').classList.add('hide')
                }
            })
        })
    }
}

const choiceFillings = () => {

    if (document.querySelector('[data-filling]')) {

        new Swiper('[data-filling]', {
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 12,
            speed: 600,
            navigation: {
                nextEl: '[data-filling-next]',
                prevEl: '[data-filling-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    spaceBetween: 12,
                }
            },
            on: {
            }
        });

        new Swiper('[data-filling-thumbs]', {
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 5,
            spaceBetween: 12,
            speed: 600,
            navigation: {
                nextEl: '[data-product-thumbs-next]',
                prevEl: '[data-product-thumbs-prev]',
            },
            on: {
            }
        });

        Fancybox.bind('[data-filling-link]', {
            autoFocus: false,
            closeButton: false
        });

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-filling-thumb]')) {
                let itemNumber = event.target.closest('[data-filling-thumb]').dataset.fillingThumb
                const fillingCards = event.target.closest('[data-filling-cards]').querySelectorAll('[data-filling-card]');

                event.target.closest('[data-filling-thumbs]').querySelectorAll('[data-filling-thumb]').forEach(elem => {
                    elem.classList.remove('active')
                })
                event.target.closest('[data-filling-thumb]').classList.add('active')

                fillingCards.forEach(elem => {
                    elem.classList.remove('active');
                });
                event.target.closest('[data-filling-cards]').querySelector(`[data-filling-card="${itemNumber}"]`).classList.add('active');

            }
        })

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-filling-check]')) {
                event.preventDefault()

                let itemNum = parseInt(event.target.closest('[data-filling-check]').dataset.fillingCheck)
                let itemNumSlide = itemNum - 1
                let itemsGroup = document.querySelector('[data-filling]')
                if (event.target.closest('[data-filling-check]').classList.contains('added')) {
                    //    event.target.closest('[data-filling-check]').classList.remove('added')
                }
                else {
                    event.target.closest('[data-filling-cards]').querySelectorAll('[data-filling-check]').forEach(elem => {
                        elem.classList.remove('added')
                    })
                    event.target.closest('[data-filling-check]').classList.add('added')
                }

                itemsGroup.querySelectorAll('[data-filling-item]').forEach(elem => {
                    elem.classList.remove('active')
                })
                itemsGroup.querySelector(`[data-filling-item="${itemNum}"]`).classList.add('active');

                console.log('Slide - ' + itemNumSlide)

                event.target.closest('.product-modal').querySelector('[data-fancybox-close]').click()
            }
        })

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-filling-item]')) {
                let itemsCards = document.querySelector('[data-filling-cards]')
                let itemNum = event.target.closest('[data-filling-item]').dataset.fillingItem

                if (event.target.closest('[data-filling-item]').classList.contains('active')) {
                    //  event.target.closest('[data-filling-item]').classList.remove('active')
                }
                else {
                    event.target.closest('[data-filling]').querySelectorAll('[data-filling-item]').forEach(elem => {
                        elem.classList.remove('active')
                    })
                    event.target.closest('[data-filling-item]').classList.add('active')
                    event.target.closest('[data-info-group]').nextElementSibling.classList.remove('disabled')
                }

                itemsCards.querySelectorAll('[data-filling-card]').forEach(elem => {
                    elem.classList.remove('active')
                })
                itemsCards.querySelector(`[data-filling-card="${itemNum}"]`).classList.add('active');


                itemsCards.querySelectorAll('[data-filling-thumb]').forEach(elem => {
                    elem.classList.remove('active')
                })
                itemsCards.querySelector(`[data-filling-thumb="${itemNum}"]`).classList.add('active');
            }
        })
    }


}

isWebp()
fullVHfix()
addTouchClass()
addLoadedClass()
SmoothScroll('[data-anchor]')
Tabs()
Quantity()
Nav()
Spoilers()
customSelect()
Lng()
Search()
FileField()
product()


document.addEventListener('click', (event) => {
    if (event.target.closest('[data-phone]')) {
        event.target.closest('[data-phone]').classList.add('active')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-widget]')) {
        document.querySelector('[data-widget]').classList.toggle('open')
    }
    else if (event.target.closest('[data-widget-open]')) {
        document.querySelector('[data-widget]').classList.add('open')
    }
    else {
        document.querySelector('[data-widget]').classList.remove('open')
    }
})


document.addEventListener('click', (event) => {
    if (event.target.closest('[data-content-toggle]')) {
        event.target.closest('[data-content]').classList.toggle('open')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-works-download]')) {
        event.target.closest('[data-works]').classList.add('downloaded')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-review-download]')) {
        document.querySelector('[data-review]').classList.add('downloaded')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-cake-check]')) {
        event.preventDefault()
        event.target.closest('[data-cake-check]').classList.toggle('added')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-delivery-switcher]')) {
        event.target.closest('.cart-checkout__group').querySelectorAll('[data-delivery-switcher]').forEach(elem => {
            elem.classList.remove('active')
        })
        event.target.closest('[data-delivery-switcher]').classList.add('active')
    }
})


window.addEventListener('load',  (e) => {
    // Запуск инициализации слайдеров
    initSliders();
    choiceFillings()

    rangeInit();

    // Modal Fancybox
    Fancybox.bind("[data-fancybox]", {
        autoFocus: false,
        closeButton: false
    });

    document.querySelectorAll('[data-mask]').forEach(elem => {
        let maskPattern = elem.dataset.mask;
        IMask(elem, {
            mask: elem.dataset.mask
        });
    });

    if (document.querySelectorAll('[data-datepicker]').length) {
        document.querySelectorAll('[data-datepicker]').forEach(el => {
            new AirDatepicker(el, {
                autoClose: true,
                selectedDates: [new Date()]
            })
        });
    }

});


