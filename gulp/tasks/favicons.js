export const favicons = () => {
	return app.gulp.src(app.path.src.favicons, {  encoding: false })
		.pipe(app.gulp.dest(app.path.build.favicons))
}
